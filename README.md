Flygbiljettbokningsprogram
==========================
This software is a school assignment for basic Object-Oriented programming in Java.
The task was to build a flight-booking system with the following features:

- Register airport ( should have 3 letters and each airport should have a unique name)
- Create airline ( must have at least 6 characters and each airline should have a unique name)
- Create route ( airline, departure airport , destination airport , flight number and date. There is a maximum of one
  flights by date )
- Print all airports, airlines and routes
- Print all bookings

- Search for available flights ( between departure airport to destination airport )
- Book tickets
- Print the booking information for a ticket


Install instructions
--------------------
Run Install.java and follow the steps to setup the database.


