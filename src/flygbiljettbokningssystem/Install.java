package flygbiljettbokningssystem;

import java.util.Scanner;

public class Install {
	
	static void createConfigFile() {
		Scanner input = new Scanner(System.in);
		System.out.println("Ange uppgifterna till din databas");
		System.out.println("Hostname:");
		Config.setProperty("db_host", input.nextLine());
		Config.setProperty("db_port", "3306");
		System.out.println("Anv�ndarnamn:");
		Config.setProperty("db_username", input.nextLine());
		System.out.println("L�senord:");
		Config.setProperty("db_password", input.nextLine());
		System.out.println("Databasnamn:");
		Config.setProperty("db_name", input.nextLine());
		Config.saveToFile();
		input.close();
	}
	
	static void createDatabase() {
		System.out.println("Skapar databasen");
		String host = Config.getProperty("db_host");
		String port = Config.getProperty("db_port");
		String name = Config.getProperty("db_name");
		String user = Config.getProperty("db_username");
		String password = Config.getProperty("db_password");			
		DatabaseConnection db = new DatabaseConnection(host, port, user, password);
		db.execute(String.format("DROP database IF EXISTS %s;", name));
		db.execute(String.format("CREATE database %s;", name));		
	}
	
	static void createTables() {
		System.out.println("Skapar tabeller");
		DatabaseConnection db = DatabaseConnection.fromConfig();
		
		
		db.execute("DROP table IF EXISTS companies;");
		db.execute("DROP table IF EXISTS airports;");
		db.execute("DROP table IF EXISTS flightlines;");
		db.execute("DROP table IF EXISTS bookings;");
	
		
		db.execute("CREATE TABLE `companies` (`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(45) NULL, PRIMARY KEY (`id`));");
		db.execute("CREATE TABLE `airports` (`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(45) NULL,PRIMARY KEY (`id`));");
		db.execute("CREATE TABLE `flightlines` (`id` INT NOT NULL AUTO_INCREMENT,`company` VARCHAR(45) NULL,`date` VARCHAR(45) NULL,"
			+ "`airportStart` VARCHAR(45) NULL,`airportStop` VARCHAR(45) NULL,`flightnumber` VARCHAR(45) NULL,PRIMARY KEY (`id`));");
		db.execute("CREATE TABLE `bookings` (`id` INT NOT NULL AUTO_INCREMENT,`company` VARCHAR(45) NULL,`date` VARCHAR(45) NULL,"
			+ "`airportStart` VARCHAR(45) NULL,`airportStop` VARCHAR(45) NULL,`flightnumber` VARCHAR(45) NULL,"
			+ "`surname` VARCHAR(45) NULL,`familyname` VARCHAR(45) NULL,PRIMARY KEY (`id`));");
	}
	
	static void insertSampleData() {
		System.out.println("L�gger till exempeldata");
		
		//insert sample data companies
		Company company1 = new Company("Scandinaivian Airlines");
		company1.save();
		Company company2 = new Company("Aviation Malm�");
		company2.save();
		Company company3 = new Company("Norwegian");
		company3.save();
		
		//insert sample data airports
		Airport airport1 = new Airport("ARL");
		airport1.save();
		Airport airport2 = new Airport("�SD");
		airport2.save();
		Airport airport3 = new Airport("GOT");
		airport3.save();
		
		//insert sample data flightlines
		Flightline flightline1 = new Flightline("Scandinaivian Airlines", "ARL", "�SD", "2015-01-04", "8219" );
		flightline1.save();
		Flightline flightline2 = new Flightline("Scandinaivian Airlines", "�SD", "GOT", "2015-01-05", "9128" );
		flightline2.save();
		Flightline flightline3 = new Flightline("Norwegian", "GOT", "�SD", "2015-01-06", "1234" );
		flightline3.save();
		
		//insert sample data bookings
		Booking booking1 = new Booking("Scandinaivian Airlines", "ARL", "�SD", "2015-01-04", "8219", "Eric", "Carlsson" );
		booking1.save();
		Booking booking2 = new Booking("Scandinaivian Airlines", "�SD", "GOT", "2015-01-05", "9128", "Lucas", "Carlsson" );
		booking2.save();
		Booking booking3 = new Booking("Norwegian", "GOT", "�SD", "2015-01-06", "1234", "Dwi", "Indahsari" );
		booking3.save();
	}

	public static void main(String[] args) {
		createConfigFile();
		createDatabase();
		createTables();
		insertSampleData();
		System.out.println("Klart!");
	}

}
