package flygbiljettbokningssystem;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Ange din roll: ");
		System.out.println("Admin (1)");
		System.out.println("Resen�r (2)");

		String choice = input.next();

		if (choice.equals("1")) {
			Administrator admin = new Administrator();
			admin.run();
		}
		if (choice.equals("2")) {
			Traveller traveller = new Traveller();
			traveller.run();
		}

	}
}
