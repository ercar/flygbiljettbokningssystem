package flygbiljettbokningssystem;

import java.util.HashMap;

public class Row {

	private HashMap<String, Object> map = new HashMap<String, Object>();
   
	/*
	 * Used to set the row column and value
	 */
	public void set(String key, Object value) {
		this.map.put(key, value);
	}

	/*
	 * Used to get the value in the row
	 */
	public Object getObject(String key) {
		return this.map.get(key);
	}

	/*
	 * Used to get the column in the row
	 */
	public String getString(String key) {
		return (String) this.getObject(key);
	}
}