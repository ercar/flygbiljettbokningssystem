package flygbiljettbokningssystem;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Config {
	
	private static Properties properties = new Properties();
	private static String filename = "config.properties";

	/*
	 * This method is used to read data from the config file
	 */
	private static void loadFile() {
		InputStream input = null;
	 	try {
	 		input = new FileInputStream(filename);
	 		properties.load(input);	 					
	 	} catch (IOException ex) {
	 		ex.printStackTrace();
	 	} finally {
	 		if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	 	}
	}
	
	/*
	 * Used to return the content from the config file for the requested property
	 */
	public static String getProperty(String key) {
		if(properties.isEmpty()) {
			loadFile();
		}
		return properties.getProperty(key);
	}
	
	/*
	 * Used to change the content in the config file for the requested property
	 */
	
	public static void setProperty(String key, String value) {
		properties.setProperty(key, value);
	}
	
	/*
	 * This method is used to store data to the config file
	 */
	public static void saveToFile()  {
		OutputStream output;
		try {
			output = new FileOutputStream(filename);
			properties.store(output, null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e ) {
			e.printStackTrace();
		}
	}
	
}
