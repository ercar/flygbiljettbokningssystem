package flygbiljettbokningssystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DatabaseConnection {

	private static DatabaseConnection configConnection;

	private Connection connection;
	private String url;
	private String username;
	private String password;

	public DatabaseConnection(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
		this.connect();
	}

	public DatabaseConnection(String host, String port, String username, String password) {
		this(String.format("jdbc:mysql://%s:%s", host, port), username, password);
	}

	public DatabaseConnection(String host, String port, String name, String username, String password) {
		this(String.format("jdbc:mysql://%s:%s/%s", host, port, name), username, password);
	}

	/*
	 * Creates a new DatabaseConnection instance with the data from the config file
	 */
	public static DatabaseConnection fromConfig() {
		if (configConnection == null) {
			String host = Config.getProperty("db_host");
			String port = Config.getProperty("db_port");
			String name = Config.getProperty("db_name");
			String user = Config.getProperty("db_username");
			String password = Config.getProperty("db_password");
			configConnection = new DatabaseConnection(host, port, name, user, password);
		}
		return configConnection;
	}
	/*
	 * Opens the connection to the database
	 */
	public void connect() {
		try {
			this.connection = DriverManager.getConnection(this.url, this.username, this.password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Used for sql inserts and updates
	public void execute(String sql) {
		try {
			Statement statement = this.connection.createStatement();
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// this.disconnect();
		}

	}
	
	/*
	 * Used to retrieve a single row from the database based on the sql statement
	 */
	public Row fetch(String sql) {
		ArrayList<Row> result = fetchAll(sql);
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}
	
	/*
	 *  Used to retrieve a list of rows from the database based on the sql statement
	 */
	public ArrayList<Row> fetchAll(String sql) {
		ArrayList<Row> result = new ArrayList<Row>();
		ResultSet rs = null;
		try {
			Statement statement = this.connection.createStatement();
			rs = statement.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				Row row = new Row();
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					String columnName = rsmd.getColumnName(i);
					row.set(columnName, rs.getObject(i));
				}
				result.add(row);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

}
