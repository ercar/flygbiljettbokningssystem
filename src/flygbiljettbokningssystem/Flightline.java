package flygbiljettbokningssystem;

import java.util.ArrayList;

public class Flightline {

	private String company;
	private String airportStart;
	private String airportStop;
	private String date;
	private String flightnumber;

	public Flightline(String company, String airportStart, String airportStop, String date, String flightnumber) {
		this.company = company;
		this.airportStart = airportStart;
		this.airportStop = airportStop;
		this.date = date;
		this.flightnumber = flightnumber;
	}

	public Company getCompany() {
		return Company.find(this.company);
	}

	public Airport getAirportStart() {
		return Airport.find(this.airportStart);
	}

	public Airport getAirportStop() {
		return Airport.find(this.airportStop);
	}

	public String getDate() {
		return this.date;
	}

	public String getFlightnumber() {
		return this.flightnumber;
	}
	/*
	 *  Checks if the typed date match a flightline-date in the database.
	 *  If it does it will return that flightline as an Flightline instance.
	 *  If it doesn't it will return null.
	 */
	public static Flightline find(String date) {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM flightlines WHERE date = '" + date + "'";
		Row row = db.fetch(sql);
		if (row == null) {
			return null;
		}
		Flightline flightline = new Flightline(row.getString("company"),
				row.getString("airportStart"), row.getString("airportStop"),
				row.getString("date"), row.getString("flightnumber"));
		return flightline;
	}
	
	/*
	 * Checks if the flightline-date already exists in the database.
	 * If it does it returns true.
	 * If it doesn't it returns false.
	 */
	public static boolean exists(String date) {
		return find(date) != null;
	}
	
	/*
	 * Used to return all flightlines from the database.
	 */
	public static ArrayList<Flightline> all() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM flightlines";
		ArrayList<Row> rows = db.fetchAll(sql);
		ArrayList<Flightline> flightlines = new ArrayList<Flightline>();
		for (Row row : rows) {
			Flightline flightline = new Flightline(row.getString("company"),
					row.getString("airportStart"),
					row.getString("airportStop"), row.getString("date"),
					row.getString("flightnumber"));
			flightlines.add(flightline);
		}
		return flightlines;
	}
	/*
	 * Used to return all flightlines that contains the selected airportStart and the selected airportStop
	 */
	public static ArrayList<Flightline> between(Airport Start, Airport Stop) {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM flightlines WHERE" + " airportStart = '"
				+ Start + "' AND airportStop = '" + Stop + "'";
		ArrayList<Row> rows = db.fetchAll(sql);
		ArrayList<Flightline> flightlines = new ArrayList<Flightline>();
		for (Row row : rows) {
			Flightline flightline = new Flightline(row.getString("company"),
					row.getString("airportStart"),
					row.getString("airportStop"), row.getString("date"),
					row.getString("flightnumber"));
			flightlines.add(flightline);
		}
		return flightlines;
	}

	public void save() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "INSERT INTO flightlines (`company`,`airportStart`,`airportStop`,`date`, `flightnumber`)"
				+ " VALUES ('%s', '%s', '%s', '%s', '%s')";
		String query = String.format(sql, this.company, this.airportStart,
				this.airportStop, this.date, this.flightnumber);
		db.execute(query);

	}

}
