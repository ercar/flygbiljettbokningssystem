package flygbiljettbokningssystem;

import java.util.Random;

public class Flightnumber {

	/*
	 * Creates a flightnumber
	 */
	public static String generate() {
		Random random = new Random();
		int number = random.nextInt(9998) + 1;
		return String.valueOf(number);
	}
}
