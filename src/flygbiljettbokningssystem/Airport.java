package flygbiljettbokningssystem;

import java.util.ArrayList;

public class Airport {

	private String name;

	public Airport(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	/*
	  *  Checks if the typed airport-name match an airport in the database.
	  *  If it does it will return that airport as an Airport instance.
	  *  If it doesn't it will return null.
	  */
	public static Airport find(String name) {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT name FROM airports WHERE name = '" + name + "'";
		Row row = db.fetch(sql);
		if (row == null) {
			return null;
		}
		String n = row.getString("name");
		Airport airport = new Airport(n);
		return airport;
	}
	/*
	 * Checks if the airport-name already exists in the database.
	 * If it does it returns true.
	 * If it doesn't it returns false.
	 */
	public static boolean exists(String name) {
		return find(name) != null;
	}
	/*
	 * Used to return all airports from the database.
	 */
	public static ArrayList<Airport> all() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM airports";
		ArrayList<Row> rows = db.fetchAll(sql);
		ArrayList<Airport> airports = new ArrayList<Airport>();
		for (Row row : rows) {
			String n = row.getString("name");
			Airport airport = new Airport(n);
			airports.add(airport);
		}
		return airports;
	}

	public void save() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "INSERT INTO airports (`name`) VALUES ('" + this.name
				+ "');";
		db.execute(sql);
	}

	public String toString() {
		return this.name;
	}
}
