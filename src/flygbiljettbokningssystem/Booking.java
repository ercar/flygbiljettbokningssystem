package flygbiljettbokningssystem;

import java.util.ArrayList;

public class Booking {

	private String company;
	private String airportStart;
	private String airportStop;
	private String date;
	private String flightnumber;
	private String surname;
	private String familyname;

	public Booking(String company, String airportStart, String airportStop, String date, String flightnumber, String surname, String familyname) {
		this.company = company;
		this.airportStart = airportStart;
		this.airportStop = airportStop;
		this.date = date;
		this.flightnumber = flightnumber;
		this.surname = surname;
		this.familyname = familyname;
	}

	public Company getCompany() {
		return Company.find(this.company);
	}

	public Airport getAirportStart() {
		return Airport.find(this.airportStart);
	}

	public Airport getAirportStop() {
		return Airport.find(this.airportStop);
	}

	public String getDate() {
		return this.date;
	}

	public String getFlightnumber() {
		return this.flightnumber;
	}

	public String getSurname() {
		return this.surname;
	}

	public String getFamilyname() {
		return this.familyname;
	}
	/*
	 * Used to return all bookings from the database.
	 */
	public static ArrayList<Booking> all() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM bookings";
		ArrayList<Row> rows = db.fetchAll(sql);
		ArrayList<Booking> bookings = new ArrayList<Booking>();
		for (Row row : rows) {
			Booking booking = new Booking(row.getString("company"),
					row.getString("airportStart"),
					row.getString("airportStop"), row.getString("date"),
					row.getString("flightnumber"), row.getString("surname"),
					row.getString("familyname"));
			bookings.add(booking);
		}
		return bookings;
	}

	public void save() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "INSERT INTO bookings (`company`,`airportStart`,`airportStop`,`date`, `flightnumber`, `surname`, `familyname` )"
				+ " VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
		String query = String.format(sql, this.company, this.airportStart,
				this.airportStop, this.date, this.flightnumber, this.surname,
				this.familyname);
		db.execute(query);

	}

}
