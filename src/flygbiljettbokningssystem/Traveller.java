package flygbiljettbokningssystem;

import java.util.ArrayList;
import java.util.Scanner;

public class Traveller {

	private Scanner input = new Scanner(System.in);
	Administrator admin = new Administrator();
	private String surname;
	private String familyname;

	public void run() {
		System.out.println("Du �r nu inloggad som Resen�r.");

		System.out.println("Ange ditt f�rnamn");
		surname = input.next();

		System.out.println("Ange ditt efternamn");
		familyname = input.next();

		while (true) {
			System.out.println("Hur vill du s�ka din resa?");
			System.out.println("S�k p� flygbolag (1)");
			System.out.println("S�k p� startflygplats och destinationsflygplats (2)");

			String choice = input.next();

			if (choice.equals("1")) {
				travelByCompany();
			}

			if (choice.equals("2")) {
				travelByAirport();
			}
		}
	}

	public void travelByAirport() {
		Airport airportStart = null;
		boolean getInput = true;
		while (getInput) {
			System.out.println("Vart vill du resa ifr�n?");
			admin.printAirports();
			String name = input.next();
			airportStart = Airport.find(name);
			if (airportStart == null) {
				System.err.println("Flygplatsen hittades inte.");
				System.out.println(" ");
			}
			else {getInput = false;}
		}
		
		Airport airportStop = null;
		getInput = true;
		while (getInput){
			System.out.println("Vart vill du resa till?");
			admin.printAirports();
			String name = input.next();
			airportStop = Airport.find(name);
			if (airportStop == null) {
				System.err.println("Flygplatsen hittades inte.");
				System.out.println(" ");
			}
			else {getInput = false;}
		}

		System.out.println("Nedan �r alla tillg�ngliga flyg med vald start- och destinationsflygplats: ");
		ArrayList<Flightline> flightlines = Flightline.between(airportStart, airportStop);
		if (flightlines.isEmpty()) {
			System.err.println("Hittade inga tillg�ngliga flyg efter s�kkriteriet.");
			this.closing();
		}
		for (Flightline flightline : flightlines) {
			System.out.print("Flygnummer: " + flightline.getFlightnumber());
			System.out.println(", ");
			System.out.print("Flygbolag: " + flightline.getCompany().getName());
			System.out.println(", ");
			System.out.print("Avresa: " + flightline.getAirportStart().getName());
			System.out.println(", ");
			System.out.print("Destination: " + flightline.getAirportStop().getName());
			System.out.println(", ");
			System.out.println("Datum: " + flightline.getDate());
			System.out.println(" ");
		}

		createBooking();

	}

	public void travelByCompany() {
		admin.printCompanies();
		System.out.println(" ");
		System.out.println("skriv namnet p� ett av flygbolaget f�r att lista deras flyglinjer");

		String name = input.next();
		Company company = new Company(name);

		ArrayList<Flightline> flightlines = company.getFlightlines();
		if (flightlines.isEmpty()) {
			System.err.println("Hittade inga tillg�ngliga flyg efter s�kkriteriet.");
			this.closing();
		}
		for (Flightline flightline : flightlines) {
			System.out.print("Flygnummer: " + flightline.getFlightnumber());
			System.out.println(", ");
			System.out.print("Flygbolag: " + company.getName());
			System.out.println(", ");
			System.out.print("Avresa: " + flightline.getAirportStart().getName());
			System.out.println(", ");
			System.out.print("Destination: " + flightline.getAirportStop().getName());
			System.out.println(", ");
			System.out.println("Datum: " + flightline.getDate());
			System.out.println(" ");
		}
		
		createBooking();
	}

	public void createBooking() {
		System.out.println("Ange ett av datumen i formatet yyy-mm-dd om du vill g� vidare och boka din resa.");
		System.out.println("Eller skriv 'avbryt' om du inte vill boka en resa.");

		String choice = input.next();

		if (choice.equals("avbryt")) {
			this.closing();
		}

		Flightline flightline = Flightline.find(choice);
		if (flightline == null) {
			System.err.println("Du har inte valt n�gon av de tillg�ngliga resorna.");
			this.closing();
		}
		Company company = flightline.getCompany();
		Airport airportStart = flightline.getAirportStart();
		Airport airportStop = flightline.getAirportStop();
		String date = flightline.getDate();
		String flightnumber = flightline.getFlightnumber();

		Booking booking = new Booking(company.getName(),
				airportStart.getName(), airportStop.getName(), date,
				flightnumber, surname, familyname);
		
		System.out.println("Bekr�fta bokning av resa:");
		System.out.println("F�rnamn: " + booking.getSurname());
		System.out.println("Efternamn: " + booking.getFamilyname());
		System.out.println("Flygnummer: " + booking.getFlightnumber());
		System.out.println("Datum resa: " + booking.getDate());
		System.out.print("Fr�n flygplats: " + booking.getAirportStart());
		System.out.print(" ");
		System.out.println("Till flygplats: " + booking.getAirportStop());
		System.out.println(" ");

		boolean getInput = true;
		while (getInput) {
			System.out.println("Vill du boka resan? Bekr�fta(1) Avbryt(2)");

			choice = input.next();

			if (choice.equals("1")) {
				System.out.println("Din resa �r nu bokad.");
				booking.save();
				this.closing();	
			}

			if (choice.equals("2")) {
			this.closing();
				
			}

		}

	}
	public void closing() {
		Scanner input = new Scanner(System.in);
		String choice;
		System.out.println(" ");
		System.out.println("Boka ny resa (1)");
		System.out.println("Avsluta programmet (2)");
		choice = input.next();
		
		if (choice.equals("1")) {
			run();
		}
		
		if (choice.equals("2")) {
			System.exit(0);
		}
	}
}
