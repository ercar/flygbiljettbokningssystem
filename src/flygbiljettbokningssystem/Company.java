package flygbiljettbokningssystem;

import java.util.ArrayList;

public class Company {

	private String name;

	public Company(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	
	 /*
	  *  Checks if the typed company-name match a company in the database.
	  *  If it does it will return that company as an Company instance.
	  *  If it doesn't it will return null.
	  */
	public static Company find(String name) {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM companies WHERE name = '" + name + "'";
		Row row = db.fetch(sql);
		if (row == null) {
			return null;
		}
		String n = row.getString("name");
		Company company = new Company(n);
		return company;
	}
	
	/*
	 * Checks if the company-name already exists in the database.
	 * If it does it returns true.
	 * If it doesn't it returns false.
	 */
	public static boolean exists(String name) {
		return find(name) != null;
	}
	
	/*
	 * Used to return all companies from the database.
	 */
	public static ArrayList<Company> all() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM companies";
		ArrayList<Row> rows = db.fetchAll(sql);
		ArrayList<Company> companies = new ArrayList<Company>();
		for (Row row : rows) {
			String n = row.getString("name");
			Company company = new Company(n);
			companies.add(company);
		}
		return companies;
	}
	
	/*
	 * Used to return all flightlines that contains this company name.
	 */
	public ArrayList<Flightline> getFlightlines() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "SELECT * FROM flightlines WHERE company = '" + this.name + "'";
		ArrayList<Row> rows = db.fetchAll(sql);
		ArrayList<Flightline> flightlines = new ArrayList<Flightline>();
		for (Row row : rows) {
			Flightline flightline = new Flightline(row.getString("Company"),
					row.getString("airportStart"),
					row.getString("airportStop"), row.getString("date"),
					row.getString("flightnumber"));
			flightlines.add(flightline);
		}
		return flightlines;
	}

	public void save() {
		DatabaseConnection db = DatabaseConnection.fromConfig();
		String sql = "INSERT INTO companies (`name`) VALUES ('" + this.name + "');";
		db.execute(sql);
	}

	public String toString() {
		return this.name;
	}
}
