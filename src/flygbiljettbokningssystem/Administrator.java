package flygbiljettbokningssystem;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Administrator {

	private Scanner input = new Scanner(System.in);

	public void run() {

		System.out.println("Du �r nu inloggad som Admin");
		while (true) {
			System.out.println("MENY ADMIN - G�r ditt val:");
			System.out.println("Skapa flygplats (1)");
			System.out.println("Skapa flygbolag (2)");
			System.out.println("Skapa flyglinje (3)");
			System.out.println("Visa alla flygplatser (4)");
			System.out.println("Visa alla flygbolag (5)");
			System.out.println("Visa alla flyglinjer (6)");
			System.out.println("Visa alla bokningar (7)");
			System.out.println("Testa programmet som resen�r (8)");
			System.out.println("Avsluta programmet (9)");

			String choice = input.next();

			if (choice.equals("1")) {
				this.createAirport();
				this.closing();
			}

			if (choice.equals("2")) {
				this.createCompany();
				this.closing();
			}

			if (choice.equals("3")) {
				this.createFlightline();
				this.closing();
			}

			if (choice.equals("4")) {
				this.printAirports();
				this.closing();
			}

			if (choice.equals("5")) {
				this.printCompanies();
				this.closing();
			}

			if (choice.equals("6")) {
				this.printFlightlines();
				this.closing();
			}

			if (choice.equals("7")) {
				this.printBookings();
				this.closing();
			}

			if (choice.equals("8")) {
				Traveller traveller = new Traveller();
				traveller.run();
			}

			if (choice.equals("9")) {
				System.exit(0);
			}
		}
	}

	public void createAirport() {
		System.out.println("Ange flygplatsens namn (M�ste vara tre bokst�ver) ");
		String name = input.next();
		Pattern airportPattern = Pattern.compile("^([A-�]{3})$");
		if (!airportPattern.matcher(name).matches()) {
			System.err.println("Felaktigt angivet flyplatsnamn.");
			closing();
		}
		if (Airport.exists(name)) {
			System.err.println("Flygplatsen finns redan");
			closing();
		}
		Airport airport = new Airport(name);
		airport.save();
		System.out.println("Flygplats tillagd.");
	}

	public void createCompany() {
		System.out.println("Ange flygbolagets namn (M�ste b�rja p� stor bokstav och inneh�lla minst 6 bokst�ver): ");
		String name = input.next();
		Pattern companyPattern = Pattern.compile("^([A-�][a-�]{5,})");
		if (!companyPattern.matcher(name).matches()) {
			System.err.println("Felaktigt angivet flygbolagsnamn.");
			closing();
		}
		if (Company.exists(name)) {
			System.err.println("Flygbolaget finns redan");
			closing();
		}
		Company company = new Company(name);
		company.save();
		System.out.println("Flygbolag tillagt.");
	}

	public void createFlightline() {
		System.out.println("Ange namnet p� flygbolag som ska kopplas till flyglinjen.");
		printCompanies();
		String name = input.next();
		Company company = Company.find(name);
		if (company == null) {
			System.err.println("Flygbolaget hittades inte.");
			closing();
		}
		System.out.println("Ange namnet p� flygplats f�r flyglinjens start.");
		printAirports();
		name = input.next();
		Airport airportStart = Airport.find(name);
		if (airportStart == null) {
			System.err.println("Flygplatsen hittades inte.");
			closing();
		}
		System.out.println("Ange namnet p� flygplats f�r flyglinjens destination.");
		printAirports();
		name = input.next();
		Airport airportStop = Airport.find(name);
		if (airportStop == null) {
			System.err.println("Flygplatsen hittades inte.");
			closing();
		}
		if (airportStop.getName().equals(airportStart.getName())) {
			System.err.println("Start och destination kan inte vara samma flygplats.");
			closing();
		}
		System.out.println("Vilket datum g�ller flyglinjen. Ange i formatet yyyy-mm-dd.");
		String date = input.next();
		Pattern datePattern = Pattern.compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$");
		if (!datePattern.matcher(date).matches()) {
			System.err.println("Felaktigt angivet datum.");
			closing();
		}
		if (Flightline.exists(date)) {
			System.err.println("Det g�r redan ett flyg p� det datumet. Max ett flyg per datum.");
			closing();
		}

		String flightnumber = Flightnumber.generate();
		Flightline flightline = new Flightline(company.getName(),
				airportStart.getName(), airportStop.getName(), date,
				flightnumber);
		flightline.save();

		System.out.println("Flyglinjen har skapats: " + airportStart.getName() + "-" + airportStop.getName());
		System.out.println("Datum: " + date);
		System.out.println("Flygnummer: " + flightnumber);
		System.out.println("Flygbolag: " + company.getName());
	}

	public void printAirports() {
		System.out.println("Nedan �r alla registrerade flygplatser: ");
		ArrayList<Airport> airports = Airport.all();
		for (Airport airport : airports) {
			System.out.println(airport.getName());
		}
	}

	public void printCompanies() {
		System.out.println("Nedan �r alla registrerade flygbolag: ");
		ArrayList<Company> companies = Company.all();
		for (Company company : companies) {
			System.out.println(company.getName());
		}
	}

	public void printFlightlines() {
		System.out.println("Nedan �r alla registrerade flyglinjer: ");
		System.out.println(" ");
		ArrayList<Flightline> flightlines = Flightline.all();
		for (Flightline flightline : flightlines) {
			System.out.print("Flygnummer: " + flightline.getFlightnumber());
			System.out.println(", ");
			System.out.print("Flygbolag: " + flightline.getCompany().getName());
			System.out.println(", ");
			System.out.print("Avresa: "
					+ flightline.getAirportStart().getName());
			System.out.println(", ");
			System.out.print("Destination: "
					+ flightline.getAirportStop().getName());
			System.out.println(", ");
			System.out.println("Datum: " + flightline.getDate());
			System.out.println(" ");

		}
	}

	public void printBookings() {
		System.out.println("Nedan �r alla registrerade bokningar: ");
		System.out.println(" ");
		ArrayList<Booking> bookings = Booking.all();
		for (Booking booking : bookings) {
			System.out.print("F�rnamn: " + booking.getSurname());
			System.out.println(", ");
			System.out.print("Efternamn: " + booking.getFamilyname());
			System.out.println(", ");
			System.out.print("Flygnummer: " + booking.getFlightnumber());
			System.out.println(", ");
			System.out.print("Flygbolag: " + booking.getCompany().getName());
			System.out.println(", ");
			System.out.print("Avresa: " + booking.getAirportStart().getName());
			System.out.println(", ");
			System.out.print("Destination: " + booking.getAirportStop().getName());
			System.out.println(", ");
			System.out.println("Datum: " + booking.getDate());
			System.out.println(" ");

		}
	}

	public void closing() {
		Scanner input = new Scanner(System.in);
		String choice;
		System.out.println(" ");
		System.out.println("Tillbaka till huvudmenyn (1)");
		System.out.println("Avsluta programmet (2)");
		choice = input.next();

		if (choice.equals("1")) {
			run();
		}

		if (choice.equals("2")) {
			System.exit(0);
		}
	}
}
